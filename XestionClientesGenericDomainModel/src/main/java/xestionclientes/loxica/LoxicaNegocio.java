package xestionclientes.loxica;

import java.util.ArrayList;
import java.util.List;
import xestionclientes.dto.Alumno;

public class LoxicaNegocio {
    private static List<Alumno> listaAlumnos = new ArrayList<>();

    public static void engadirCliente(Alumno alumno) {
        listaAlumnos.add(0, alumno);
    }
    
    public static void borrarCliente(Alumno alumno) {
        listaAlumnos.remove(alumno);
    }
    
    public static void modificarCliente(int position, Alumno newAlumno) {
        listaAlumnos.set(position, newAlumno);
    }

    public static List<Alumno> getListaAlumnos() {
        return listaAlumnos;
    }
}
