/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xestionclientes.dto;

import xestionclientes.gui.PantallaPrincipal;
import java.util.Date;

/**
 *
 * @author a19dariocp
 */
public class Alumno {
    private String nome;
    private Date dataAlta;
    private String curso;

    public Alumno(String nome, String curso, Date dataMatriculacion) {
        this.nome = nome;
        this.dataAlta = dataMatriculacion;
        this.curso = curso;
    }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataAlta() {
		return dataAlta;
	}

	public void setDataAlta(Date dataAlta) {
		this.dataAlta = dataAlta;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String[] toArray() {
        String[] array = new String[4];
        array[0] = nome;
		array[1] = curso;
		array[2] = PantallaPrincipal.dateFormat.format(dataAlta);
        return array;
    }    
}
