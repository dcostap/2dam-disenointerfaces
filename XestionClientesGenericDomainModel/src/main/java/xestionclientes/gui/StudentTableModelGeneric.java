/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xestionclientes.gui;

import java.awt.Component;
import xestionclientes.dto.Alumno;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * @author Darius
 */
public class StudentTableModelGeneric extends GenericDomainTableModel<Alumno> {   

    public StudentTableModelGeneric() {
        
    }

    
    
    @Override
    public Class<?> getColumnClass(int i) {
        switch (i) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return Date.class;
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return true;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        switch (i1) {
            case 0:
                return getDomainObject(i).getNome();
            case 1:
                return getDomainObject(i).getCurso();
            case 2:
                return getDomainObject(i).getDataAlta();
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Nome";
            case 1:
                return "Curso";
            case 2:
                return "Data de Matrícula";
        }
        return null;
    }

    @Override
    public void setValueAt(Object o, int i, int i1) {
        switch (i1) {
            case 0:
                getDomainObject(i).setNome((String) o);
                break;
            case 1:
                getDomainObject(i).setCurso((String) o);
                break;
            case 2:
                getDomainObject(i).setDataAlta((Date) o);
                break;
        }
    }

}
