/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta.exercisebeans;

import java.awt.Color;
import java.io.Serializable;
import javax.swing.JButton;

/**
 *
 * @author a19dariocp
 */
public class CustomButton extends JButton implements Serializable {

    private ColorProperty colorProperty;
    
    public CustomButton() {
    }

    public ColorProperty getColorProperty() {
        return colorProperty;
    }

    public void setColorProperty(ColorProperty colorProperty) {
        this.colorProperty = colorProperty;
        
        setOpaque(true);
        setForeground(colorProperty.getTextColor());
        setBackground(colorProperty.getBackgroundColor());
        setBorderPainted(false);
    }    
}
