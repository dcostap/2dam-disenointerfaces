/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta.calculadoragridlayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.text.DefaultCaret;

/**
 *
 * @author a19dariocp
 */
public class CalculatorGrid extends javax.swing.JFrame {

    enum Operation {
        ADD {
            @Override
            public String toString() {
                return "+";
            }
        },
        SUBTRACT {
            @Override
            public String toString() {
                return "-";
            }
        },
        MULTIPLY {
            @Override
            public String toString() {
                return "*";
            }
        },
        DIVIDE {
            @Override
            public String toString() {
                return "/";
            }
        }
    }

    private Long num1 = null;
    private Operation operation;
    private Long num2 = null;
    private Float result = null;

    /**
     * Creates new form CalculatorGrid
     */
    public CalculatorGrid() {
        initComponents();

        jTextAreaResult.setEditable(false);
        DefaultCaret caret = (DefaultCaret) jTextAreaResult.getCaret();
        caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        updateLabel();

        HashMap<Integer, JButton> numberButtons = new HashMap<>();
        numberButtons.put(0, jButton0);
        numberButtons.put(1, jButton1);
        numberButtons.put(2, jButton2);
        numberButtons.put(3, jButton3);
        numberButtons.put(4, jButton4);
        numberButtons.put(5, jButton5);
        numberButtons.put(6, jButton6);
        numberButtons.put(7, jButton7);
        numberButtons.put(8, jButton8);
        numberButtons.put(9, jButton9);

        for (HashMap.Entry<Integer, JButton> entry : numberButtons.entrySet()) {
            entry.getValue().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (num1 == null || operation == null) {
                        int newNumber = entry.getKey();
                        num1 = Long.parseLong(String.valueOf(num1 == null ? 0 : num1) + String.valueOf(newNumber));
                    } else {
                        int newNumber = entry.getKey();
                        num2 = Long.parseLong(String.valueOf(num2 == null ? 0 : num2) + String.valueOf(newNumber));
                    }

                    updateLabel();
                }
            });
        }

        jButtonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onPressOperation(Operation.ADD);
            }
        });

        jButtonSubtract.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onPressOperation(Operation.SUBTRACT);
            }
        });

        jButtonMultiply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onPressOperation(Operation.MULTIPLY);
            }
        });

        jButtonDivide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onPressOperation(Operation.DIVIDE);
            }
        });

        jButtonCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (num1 != null && num2 != null && operation != null) {
                    switch (operation) {
                        case ADD:
                            result = (float) num1 + num2;
                            break;
                        case SUBTRACT:
                            result = (float) num1 - num2;
                            break;
                        case MULTIPLY:
                            result = (float) num1 * num2;
                            break;
                        case DIVIDE:
                            result = (float) num1 / num2;
                            break;
                    }
                } else if (num1 != null) {
                    result = (float) num1;
                } else {
                    result = 0f;
                }

                updateLabel();
                num1 = null;
                num2 = null;
                operation = null;
            }
        });
    }

    private void onPressOperation(Operation op) {
        if (num1 != null) {
            operation = op;
        }
        updateLabel();
    }

    private void updateLabel() {
        StringBuilder text = new StringBuilder();
        text.append(num1 == null ? " " : num1);
        text.append((operation == null || num1 == null) ? "   " : " " + operation + " ");
        text.append(num2 == null ? " " : num2);

        text.append("\n");
        if (result != null) {
            text.append("= " + String.format("%.5f", result));
        }
        jTextAreaResult.setText(text.toString());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButtonDivide = new javax.swing.JButton();
        jButtonSubtract = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButtonMultiply = new javax.swing.JButton();
        jButtonAdd = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton0 = new javax.swing.JButton();
        jButtonCalculate = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaResult = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(3, 5));

        jButton7.setText("7");
        jPanel1.add(jButton7);

        jButton8.setText("8");
        jPanel1.add(jButton8);

        jButton9.setText("9");
        jPanel1.add(jButton9);

        jButtonDivide.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        jButtonDivide.setText("/");
        jPanel1.add(jButtonDivide);

        jButtonSubtract.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        jButtonSubtract.setText("-");
        jPanel1.add(jButtonSubtract);

        jButton4.setText("4");
        jPanel1.add(jButton4);

        jButton5.setText("5");
        jPanel1.add(jButton5);

        jButton6.setText("6");
        jPanel1.add(jButton6);

        jButtonMultiply.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        jButtonMultiply.setText("*");
        jPanel1.add(jButtonMultiply);

        jButtonAdd.setFont(new java.awt.Font("Cantarell", 1, 18)); // NOI18N
        jButtonAdd.setText("+");
        jPanel1.add(jButtonAdd);

        jButton1.setText("1");
        jPanel1.add(jButton1);

        jButton2.setText("2");
        jPanel1.add(jButton2);

        jButton3.setText("3");
        jPanel1.add(jButton3);

        jButton0.setText("0");
        jPanel1.add(jButton0);

        jButtonCalculate.setBackground(new java.awt.Color(213, 244, 252));
        jButtonCalculate.setFont(new java.awt.Font("Cantarell", 1, 22)); // NOI18N
        jButtonCalculate.setText("Calcular");
        jPanel1.add(jButtonCalculate);

        jTextAreaResult.setColumns(20);
        jTextAreaResult.setFont(new java.awt.Font("Cantarell", 1, 20)); // NOI18N
        jTextAreaResult.setRows(2);
        jTextAreaResult.setText("testtestest");
        jTextAreaResult.setMargin(new java.awt.Insets(5, 5, 5, 5));
        jScrollPane1.setViewportView(jTextAreaResult);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
        // (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the default
         * look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalculatorGrid.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalculatorGrid.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalculatorGrid.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalculatorGrid.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        }
        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalculatorGrid().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton0;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonCalculate;
    private javax.swing.JButton jButtonDivide;
    private javax.swing.JButton jButtonMultiply;
    private javax.swing.JButton jButtonSubtract;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaResult;
    // End of variables declaration//GEN-END:variables
}
