/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

/**
 * FXML Controller class
 *
 * @author a19dariocp
 */
public class TableController implements Initializable {

    @FXML
    private TableView<Alumno> table;
    @FXML
    private TextField fieldName;
    @FXML
    private TextField fieldSurname;
    @FXML
    private TextField fieldEmail;

    final ObservableList<Alumno> data = FXCollections.observableArrayList(
            new Alumno("Pepe", "Domínguez", "pepe@gmail.com")
    );
    @FXML
    private TableColumn<Alumno, String> column1;
    @FXML
    private TableColumn<Alumno, String> column2;
    @FXML
    private TableColumn<Alumno, String> column3;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));
        column1.setCellFactory(TextFieldTableCell.forTableColumn());
        
        column2.setCellValueFactory(new PropertyValueFactory<>("surname"));
        column2.setCellFactory(TextFieldTableCell.forTableColumn());
        
        column3.setCellValueFactory(new PropertyValueFactory<>("email"));
        column3.setCellFactory(TextFieldTableCell.<Alumno>forTableColumn());
        
        table.setItems(data);
        table.setEditable(true);
    }

    @FXML
    private void addAlumnoAction(ActionEvent event) {
        Alumno alumno = new Alumno(
                fieldName.getText(),
                fieldSurname.getText(),
                fieldEmail.getText());
        data.add(alumno);
    }

}
