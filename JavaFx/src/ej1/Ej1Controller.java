/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

/**
 * FXML Controller class
 *
 * @author Darius
 */
public class Ej1Controller implements Initializable {

    @FXML
    Button button;
    @FXML
    Label label;
    @FXML
    private Button buttonExit;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        buttonExit.setTooltip(new Tooltip("Pulsa para salir"));
        buttonExit.setMnemonicParsing(true);
    }    

    @FXML
    private void onButtonAction(ActionEvent event) {
        label.setText("Benvida");
    }

    @FXML
    private void onButtonExitAction(ActionEvent event) {
        Platform.exit();
    }
    
}
