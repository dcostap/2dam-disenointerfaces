/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alerts;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author a19dariocp
 */
public class LoginFormController implements Initializable {

    @FXML
    private PasswordField fieldPassword;
    @FXML
    private TextField fieldName;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AlertsController.loginFormController = this;
    }    

    public PasswordField getFieldPassword() {
        return fieldPassword;
    }

    public TextField getFieldName() {
        return fieldName;
    }
    
    
    
}
