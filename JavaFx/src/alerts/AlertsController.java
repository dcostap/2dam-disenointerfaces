/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alerts;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author a19dariocp
 */
public class AlertsController implements Initializable {

    static LoginFormController loginFormController;

    @FXML
    private HBox hbox;
    @FXML
    private Label labelChoiceResult;
    @FXML
    private Label labelConfirmResult;
    @FXML
    private Label labelTextInput;

    Image icon;

    public AlertsController() {
        this.icon = new Image("resources/small_icon.png");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void onButtonActionInfo(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.showAndWait();
    }

    @FXML
    private void onButtonActionWarning(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setHeaderText("");
        alert.showAndWait();

    }

    @FXML
    private void onButtonActionError(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Custom Header");
        alert.showAndWait();
    }

    @FXML
    private void onButtonActionCustomError(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Ficheiro non encontrado");
        Label label = new Label("O ficheiro indicado non existe!");

        alert.getDialogPane().setExpandableContent(label);
        alert.showAndWait();
    }

    private String[] choices = new String[]{"Option1", "Option2"};

    @FXML
    private void onButtonActionChoices(ActionEvent event) {
        ChoiceDialog cd = new ChoiceDialog(choices[0], choices);
        cd.setHeaderText("Choose option");
        cd.setContentText("Option: ");
        Optional<String> result = cd.showAndWait();
        if (result.isPresent()) {
            labelChoiceResult.setText(result.get());
        } else {
            labelChoiceResult.setText("");
        }
    }

    @FXML
    private void onButtonActionConfirmDialog(ActionEvent event) {
        ButtonType bt1 = new ButtonType("1");
        ButtonType bt2 = new ButtonType("2");
        ButtonType bt3 = new ButtonType("3");

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", bt1, bt2, bt3);
        alert.setHeaderText("Press a button if you dare");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            labelConfirmResult.setText(result.get().getText());
        }
    }

    @FXML
    private void onButtonActionTextInput(ActionEvent event) {
        TextInputDialog tid = new TextInputDialog("Teclea un texto");
        Optional<String> result = tid.showAndWait();
        if (result.isPresent()) {
            labelTextInput.setText(result.get());
        } else {
            labelTextInput.setText("");
        }
    }

    @FXML
    private void onButtonActionLogin(ActionEvent event) {

        try {
            Parent root;
            root = FXMLLoader.load(Alerts.class.getResource("loginForm.fxml"));
            Scene scene = new Scene(root, 300, 500);
            Dialog dialog = new Dialog();
            DialogPane dialogPane = new DialogPane();
            dialogPane.setContent(root);

            dialog.setDialogPane(dialogPane);

            dialogPane.getButtonTypes().add(ButtonType.CANCEL);

            final ButtonType buttonType = new ButtonType("Login");
            dialogPane.getButtonTypes().add(buttonType);

            // disable login button when name field is empty
            dialogPane.lookupButton(buttonType).setDisable(true); // start with it disabled
            
            loginFormController.getFieldName().textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    dialogPane.lookupButton(buttonType).setDisable(newValue.isBlank());
                }
            });

            loginFormController.getFieldName().requestFocus();

            final Stage stage = new Stage();
            dialogPane.setHeaderText("Un diálogo personalizado");
            dialog.setTitle("Login");

            dialogPane.setGraphic(new ImageView(icon));
            
            // transform the typical result of Dialog (the pressed ButtonType) into the string to be output
            // done this way to meet exercise requirements
            dialog.setResultConverter((param) -> {
                if (param == ButtonType.CANCEL) {
                    return "";
                } else {
                    return "name: " + loginFormController.getFieldName().getText()
                            + "; password: " + loginFormController.getFieldPassword().getText();
                }
            });

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                System.out.println(result);
            }
        } catch (IOException ex) {
            Logger.getLogger(AlertsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
