/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package popup_modals;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author a19dariocp
 */
public class Popup_modalsController implements Initializable {

    @FXML
    private HBox hbox;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }   
    
    private void createPopup(Modality modality)  {
        Stage stage = new Stage();
        stage.initOwner( hbox.getScene().getWindow());
        stage.initModality(modality);
        HBox hb = new HBox();
        stage.setScene(new Scene(hb));
        stage.showAndWait();
    }

    public void onButtonActionNone(ActionEvent event) {
        createPopup(Modality.NONE);
    }

    public void onButtonActionWinModal(ActionEvent event) {
        createPopup(Modality.WINDOW_MODAL);
    }

    public void onButtonActionAppModal(ActionEvent event) {
        createPopup(Modality.APPLICATION_MODAL);
    }
}
