/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formulario;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Darius
 */
public class FormularioController implements Initializable {

    @FXML
    private Button login_button;
    @FXML
    private Label bottom_label;
    @FXML
    private TextField username_field;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bottom_label.setText("");
    }    

    @FXML
    private void loginAction(ActionEvent event) {
        bottom_label.setText("Botón pulsado");
    }
    
}
