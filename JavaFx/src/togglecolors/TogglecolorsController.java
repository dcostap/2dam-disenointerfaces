/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package togglecolors;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * FXML Controller class
 *
 * @author a19dariocp
 */
public class TogglecolorsController implements Initializable {

    @FXML
    private ToggleButton vermello;
    @FXML
    private ToggleGroup group;
    @FXML
    private ToggleButton azul;
    @FXML
    private ToggleButton verde;
    @FXML
    private Rectangle rectangle;

    private Color blue = new Color(0, 0, 1, 1);
    private Color red = new Color(1, 0, 0, 1);
    private Color green = new Color(0, 1, 0, 1);
    private Color white = new Color(1, 1, 1, 1);
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {
                ToggleButton rb = (ToggleButton) t1;
                rectangle.fillProperty().setValue(white);
                if (rb == null) return;
                if (rb.getId().equals("vermello"))  {
                    rectangle.fillProperty().setValue(red);
                } else if (rb.getId().equals("azul"))  {
                    rectangle.fillProperty().setValue(blue);
                } else if (rb.getId().equals("verde"))  {
                    rectangle.fillProperty().setValue(green);
                }
            }
        });
    }    
    
}
