/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slider;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Darius
 */
public class SliderController implements Initializable {

    @FXML
    private Slider slider;
    @FXML
    private ImageView image;

    Image speaker2;
    Image speaker1;
    Image speaker0;

    public SliderController() {
        this.speaker0 = new Image("resources/speaker0.png");
        this.speaker1 = new Image("resources/speaker1.png");
        this.speaker2 = new Image("resources/speaker2.png");
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        image.setImage(speaker0);

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                long value = t1.longValue();
                
                if (value > 75) {
                    image.setImage(speaker2);
                } else if (value > 0) {
                    image.setImage(speaker1);
                } else {
                    image.setImage(speaker0);
                }
            }
        });
    }

}
