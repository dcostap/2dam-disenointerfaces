/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkbox;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Darius
 */
public class CheckboxController implements Initializable {

    @FXML
    private CheckBox checkbox;
    @FXML
    private HBox hbox;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        checkbox.selectedProperty().addListener((ov, oldVal, newVal) -> {
            Stage stage = (Stage) hbox.getScene().getWindow();
            stage.setTitle(newVal ? "Checkbox" : "" );
        });
        
    }    
    
}
