/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progressbar;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;

/**
 * FXML Controller class
 *
 * @author a19dariocp
 */
public class ProgressbarController implements Initializable {

    @FXML
    private ProgressBar progress;
    @FXML
    private Button more;
    @FXML
    private Button less;
    @FXML
    private ProgressIndicator indicator;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        progress.progressProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                indicator.setProgress((Double) t1);
            }
        });
    }    

    @FXML
    private void onMoreAction(ActionEvent event) {
        DoubleProperty progressProp = progress.progressProperty();
        progressProp.setValue(progressProp.doubleValue() + 0.1f);
    }

    @FXML
    private void onLessAction(ActionEvent event) {
        DoubleProperty progressProp = progress.progressProperty();
        progressProp.setValue(progressProp.doubleValue() - 0.1f);
    }
    
}
