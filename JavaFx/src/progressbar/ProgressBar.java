/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progressbar;

import slider.*;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class
ProgressBar extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        // change to this:
        // Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("progressbar\\progressbar.fxml"));
        // in windows
        
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("progressbar/progressbar.fxml"));
        Scene scene = new Scene(root, 300, 200);
        stage.setScene(scene);
        stage.setTitle("");
        stage.show();
        stage.setResizable(true);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
