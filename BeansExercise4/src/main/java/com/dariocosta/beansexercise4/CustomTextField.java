/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta.beansexercise4;

import java.io.Serializable;
import javax.swing.JFormattedTextField;

/**
 *
 * @author Darius
 */
public class CustomTextField extends JFormattedTextField implements Serializable {    
    private boolean isInteger = false;    
    
    public boolean isIsInteger() {
        return isInteger;
    }

    public void setIsInteger(boolean isInteger) {
        this.isInteger = isInteger;
    }
}
