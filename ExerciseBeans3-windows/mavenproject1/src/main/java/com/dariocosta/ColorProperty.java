/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta;

import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author a19dariocp
 */
public class ColorProperty implements Serializable {
    private Color textColor;
    private Color backgroundColor;

    public ColorProperty() {
        this.textColor = Color.BLACK;
        this.backgroundColor = Color.WHITE;
    }
    
    public ColorProperty(Color textColor, Color backgroundColor) {
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
    } 
    
    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public String toString() {
        return "Text: " + textColor + ", Back: " + backgroundColor;
    }
    
    

}
