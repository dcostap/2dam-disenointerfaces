/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta;

import java.awt.Component;
import java.beans.PropertyEditorSupport;

/**
 *
 * @author a19dariocp
 */
public class ColorPropertyEditorSupport extends PropertyEditorSupport {

    private ColorPropertyPanel cpp = new ColorPropertyPanel();
    
    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return cpp;
    }    

    @Override
    public String getJavaInitializationString() {
        ColorProperty cp = cpp.getSelectedValue();
        return "new com.dariocosta.ColorProperty" + 
                "(" + 
                "new java.awt.Color(" 
                + cp.getTextColor().getRed()
                + ","
                + cp.getTextColor().getGreen()
                + ","
                + cp.getTextColor().getBlue()
                + ","
                + cp.getTextColor().getAlpha()
                + ")," + 
                "new java.awt.Color(" 
                + cp.getBackgroundColor().getRed()
                + ","
                + cp.getBackgroundColor().getGreen()
                + ","
                + cp.getBackgroundColor().getBlue()
                + ","
                + cp.getBackgroundColor().getAlpha()
                + ")" + 
                ")";
    }

    @Override
    public Object getValue() {
        return cpp.getSelectedValue();
    }
    
}
