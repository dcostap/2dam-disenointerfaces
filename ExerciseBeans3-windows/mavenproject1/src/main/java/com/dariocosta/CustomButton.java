/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import javax.swing.JButton;

/**
 *
 * @author a19dariocp
 */
public class CustomButton extends JButton implements Serializable {

    private ColorProperty color = new ColorProperty();
    private ColorProperty hoverColor = new ColorProperty();
    
    private boolean isHover = false;
    
    public CustomButton() {
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mousePressed(MouseEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                isHover = true;
                updateColors();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                isHover = false;
                updateColors();
            }
        });
    }

    public ColorProperty getColor() {
        return color;
    }

    public void setColor(ColorProperty color) {
        this.color = color;
        
        updateColors();
    }

    public ColorProperty getHoverColor() {
        return hoverColor;
    }

    public void setHoverColor(ColorProperty hoverColor) {
        this.hoverColor = hoverColor;
        updateColors();
    }    
    
    private void updateColors() {
        setOpaque(true);
        
        ColorProperty usedColor;
        if (isHover) usedColor = hoverColor;
        else usedColor = color;
        
        setForeground(usedColor.getTextColor());
        setBackground(usedColor.getBackgroundColor());
        setBorderPainted(false);
    }
}
