/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dariocosta.cardlayout;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

/**
 *
 * @author a19dariocp
 */
public class CardLayoutTest {
    private String firstPanelName = "TEXTO PARA EDITAR";
    private String secondPanelName = "BOTÓNS";
    
    public CardLayoutTest() {        
        JFrame frame = new JFrame("CardLayoutDemo");
        
        final JPanel cardLayout = new JPanel(new CardLayout());
        
        JPanel comboBoxPane = new JPanel();
        String comboBoxItems[] = { firstPanelName, secondPanelName };
        
        JComboBox cb = new JComboBox(comboBoxItems);
        
        cb.setEditable(false);
        cb.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                CardLayout cl = (CardLayout)(cardLayout.getLayout());
                
                String panelName = (String) e.getItem();
                cl.show(cardLayout, panelName);
            }
        });
        comboBoxPane.add(cb);
        
        JPanel firstPanel = new JPanel();
        JTextField tf = new JTextField("Podes editar este texto");
        tf.setMinimumSize(new Dimension(50, 20));
        firstPanel.add(tf);
        
        JPanel secondPanel = new JPanel();
        secondPanel.add(new JButton("Botón 1"));
        secondPanel.add(new JButton("Botón 2"));
        
        cardLayout.add(firstPanel, firstPanelName);
        cardLayout.add(secondPanel, secondPanelName);
        
        frame.add(comboBoxPane, BorderLayout.PAGE_START);
        frame.add(cardLayout, BorderLayout.CENTER);
        
        frame.pack();
        frame.setVisible(true);
}
    
    public static void main(String[] args) {
        /* Use an appropriate Look and Feel */
        try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new CardLayoutTest();
            }
        });
    }
}
