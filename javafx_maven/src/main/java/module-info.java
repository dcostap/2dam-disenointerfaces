module com.dariocosta.javafx_maven {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;

    opens com.dariocosta.formulario to javafx.fxml;
    exports com.dariocosta.formulario;
}
