package com.dariocosta.formulario;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class Formulario extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("formulario.fxml"));
        Scene scene = new Scene(root, 300, 200);
        stage.setScene(scene);
        stage.setTitle("");
        stage.show();
        stage.setResizable(true);
    }

    public static void main(String[] args) {
        launch();
    }

}