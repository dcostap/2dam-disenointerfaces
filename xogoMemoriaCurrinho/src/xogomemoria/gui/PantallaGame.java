/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xogomemoria.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author a19daniellr
 */
public class PantallaGame extends javax.swing.JFrame {
    
    private static String difficulty = "";
    int count = 0;
    JToggleButton first = null;
    JToggleButton second = null;
    ArrayList<JToggleButton> buttons = new ArrayList<>();
    ArrayList<ImageIcon> images = new ArrayList<>();
    PantallaPrincipal pp = new PantallaPrincipal();
    int attemps = 0;

    PantallaGame(String dificultad) {
        initComponents();
        difficulty = dificultad;
        startGame();
    }
    
    private void clickCard(java.awt.event.MouseEvent evt) {
        JToggleButton button = (JToggleButton) evt.getSource();

		System.out.println("first = " + first);
		System.out.println("second = " + second);

        if (first != null && second != null) { // xa pulsaches 2 antes, sal correndo
        	button.setSelected(false);
            return;
        }

		if (first != null && second == null) { // pulsaches o 2º
            second = button;
            second.setSelected(true);
            try {
                Runnable run = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(500);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        
                        if (first.getIcon().equals(second.getIcon())) {
                            first.setVisible(false);
                            second.setVisible(false);
                        } else {
                            first.setSelected(false);
                            second.setSelected(false);
                        }
                        
                        first = null;
                        second = null;
                    }
                };
                new Thread(run).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (first == null && second == null) { // pulsaches o 1º
            first = button;
            first.setSelected(true);
        }
    }
    
    private void startGame() {
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/cartas.png")));
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/corazon.png")));
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/fichas.png")));
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/picas.png")));
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/rombo.png")));
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/trebol.png")));
        images.add(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/back.png")));
        
        int j = 0;
        
        if (difficulty.equals("Easy") || difficulty.equals("Medium")) {
            attemps = difficulty.equals("Easy") ? 6 : 3;
            for (int i = 0; i < 8; i++) {
                JToggleButton button = new JToggleButton();
                button.setIcon(images.get(images.size() - 1));
                final ImageIcon imageFinal = images.get(j);

                button.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						clickCard(button);

						if (!button.isSelected()) {
							button.setIcon(images.get(images.size() - 1));
						} else {
							button.setIcon(imageFinal);
						}
					}
				});

                buttons.add(button);
                if (i % 2 != 0) {
                    j++;
                }
            }
        }
//		else {
//			attemps = difficulty.equals("Hard") ? 4 : 2;
//			for (int i = 0; i < 12; i++) {
//				JToggleButton button = new JToggleButton();
//				button.setIcon(images.get(images.size() - 1));
//				final ImageIcon imageFinal = images.get(j);
//				button.addChangeListener((new ChangeListener() {
//					@Override
//					public void stateChanged(ChangeEvent ce) {
//
//					}
//				}));
//				button.addMouseListener(new MouseListener() {
//					@Override
//					public void mouseClicked(MouseEvent me) {
//						System.out.println("pressed");
//						clickCard(me);
//
//						if (!button.isSelected()) {
//							button.setIcon(images.get(images.size() - 1));
//						} else {
//							button.setIcon(imageFinal);
//						}
//					}
//
//					@Override
//					public void mousePressed(MouseEvent me) {
//
//					}
//
//					@Override
//					public void mouseReleased(MouseEvent me) {
//					}
//
//					@Override
//					public void mouseEntered(MouseEvent me) {
//					}
//
//					@Override
//					public void mouseExited(MouseEvent me) {
//					}
//				});
//				buttons.add(button);
//				if (i % 2 != 0) {
//					j++;
//				}
//			}
//		}
        Collections.shuffle(buttons);
        buttons.forEach(button -> {
            jPanelDown.add(button);
        });
        jLabel1.setText(String.valueOf(attemps));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelUp = new javax.swing.JPanel();
        jLabelAttemps = new javax.swing.JLabel();
        jButtonExit = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanelDown = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(600, 400));
        setResizable(false);

        jLabelAttemps.setText("Attemps:");

        jButtonExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/xogomemoria/gui/images/exit.png"))); // NOI18N
        jButtonExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelUpLayout = new javax.swing.GroupLayout(jPanelUp);
        jPanelUp.setLayout(jPanelUpLayout);
        jPanelUpLayout.setHorizontalGroup(
            jPanelUpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUpLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelAttemps)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 391, Short.MAX_VALUE)
                .addComponent(jButtonExit, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanelUpLayout.setVerticalGroup(
            jPanelUpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUpLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelUpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonExit, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelAttemps, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanelDown.setLayout(new java.awt.GridLayout(2, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelDown, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelUp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelDown, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonExitActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                    
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PantallaGame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PantallaGame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PantallaGame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PantallaGame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PantallaGame(difficulty).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonExit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelAttemps;
    private javax.swing.JPanel jPanelDown;
    private javax.swing.JPanel jPanelUp;
    // End of variables declaration//GEN-END:variables
}
