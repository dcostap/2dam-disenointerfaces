/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicie5_6;

import java.beans.PropertyEditorSupport;
import java.awt.Component;
/**
 *
 * @author a19daniellr
 */
public class JLabelHoraPropertyEditorSupport extends PropertyEditorSupport {
    private JLabelHoraPanel jLabelPanel = new JLabelHoraPanel();

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        return jLabelPanel;
    }

    @Override
    public String getJavaInitializationString() {
        Clock clock = jLabelPanel.getSelectedValue();

        return "new exercicie5_6.Clock(" + clock.isTimeFormat24() + ",\"" + clock.getAlarmHour() + "\")";
    }

    @Override
    public Object getValue() {
        return jLabelPanel.getSelectedValue();
    }
}
