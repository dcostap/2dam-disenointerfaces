/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicie5_6;

import java.io.Serializable;
/**
 *
 * @author a19daniellr
 */
public class Clock implements Serializable{
    
    private boolean timeFormat24;
    private String alarmHour;

    @Override
    public String toString() {
        String text = alarmHour == null || alarmHour.isEmpty()?"Formato 24H: " + timeFormat24 : "Formato 24H: " + timeFormat24 + ", Alarma: " + alarmHour;
        return text;
    }

    public Clock(boolean timeFormat24, String alarmHour) {
        this.timeFormat24 = timeFormat24;
        this.alarmHour = alarmHour;
    }

    public boolean isTimeFormat24() {
        return timeFormat24;
    }

    public void setTimeFormat24(boolean timeFormat24) {
        this.timeFormat24 = timeFormat24;
    }

    public String getAlarmHour() {
        return alarmHour;
    }

    public void setAlarmHour(String alarmHour) {
        this.alarmHour = alarmHour;
    }    
}