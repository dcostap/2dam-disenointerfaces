/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicie5_6;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JLabel;

/**
 *
 * @author a19daniellr
 */
public class JLabelHora extends JLabel implements Serializable {

    Clock clock;
    private AlarmaListener alarmaListener;

    public JLabelHora() {
        if (clock == null) {
            this.clock = new Clock(true, "");
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (clock != null) {
                        setText(getTime(clock.isTimeFormat24()));
                        if (!clock.getAlarmHour().equals("")) {
                            if (clock.getAlarmHour().equals(getTime(clock.isTimeFormat24()))) {
                                if (alarmaListener != null) {
                                    alarmaListener.alarmaAction();
                                }
                            }
                        }
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public Clock getClock() {
        return clock;
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public void addAlarmaListener(AlarmaListener alarmaListener) {
        this.alarmaListener = alarmaListener;
    }

    public void removeAlarmaListener() {
        this.alarmaListener = null;
    }

    private String getTime(boolean timeFormat24) {
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        String time = "";
        String minutes = calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : String.valueOf(calendar.get(Calendar.MINUTE));
        if (timeFormat24) {
            String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + calendar.get(Calendar.HOUR_OF_DAY) : String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)));
            time = hour + ":" + minutes;
        } else {
            String hour = String.valueOf(calendar.get(Calendar.HOUR) < 10 ? "0" + calendar.get(Calendar.HOUR) : String.valueOf(calendar.get(Calendar.HOUR)));
            time = hour + ":" + minutes;
        }
        return time;
    }
}
