# informes

primeiro instalar uns plugins en netbeans

úsase a librería de javadb driver (para conexión ca BD)
    esta lib inclúe todos os jars dentro de ~/GlassFish_Server/javadb/lib
    (ruta de instalación de GlassFish_Server cando instalas en netbeans)

outra lib usada é a de JasperReports, con jars que pasa o profesor. Úsase para manexar informes desde java

pillar sqls, crear base de datos en carpeta baseDatos dentro do root do proxecto
indicar ruta absoluta no xestor de conexións de netbeans
asegurarse de que se ejecutan os sql nesa BD

en código parece que non fai falta ruta absoluta, e parece que falla si pos ruta absoluta

con ruta relativa, indicando só nome da BD parece funcionar

crear datasource, especificando a misma conexión a BD que creamos antes

crear un informe, cambiar language a groovy, escribir consulta sql etc

## fields
o resultado da consulta son os Fields aos que tes acceso na esquerda

accede a un field con $F{}

## parameters

son variables que o informe recolle de forma externa, podes pasalos por código ou si lanzas preview do informe xa os pide ao inicio

accede a un parámetro con $P{}

## grupos

click dereito en nome de report -> add report group

parece ser que a sentencia sql do informe debe ser modificada añadindo un group by co field polo que se agrupou

unha vez creado o grupo tes varias bandas especiales (group header / group footer)

arrastrando fields dentro podes facer cousas como contar o nº de clientes de cada ciudad (si estamos agrupoando por ciudad), sería arrastrar cliente_id dentro, elegir `count`, esto xa crea unha **variable** que conta

## subinforme

basicamente un informe inctrustado en un informe root, terá un parámetro que se pasa desde o root

ejemplo de consulta sql máis compleja:
`select * from orders o, employees e where o.EmployeeID=e.EmployeeID and OrderID=$P{idPedido}`

pódense diferenciar tablas con esas variables, neste caso é necesario para diferenciar o id de un e de outro para poder pillar os que teñan o mismo id

basicamente pillamos os productos co id que pasamos en un parámetro, pero tamén o employee relacionado con ese producto, o id de ese employee `$F{IDCLIENT} é pasado a un subinforme para poder mostrar info de ese employee; o subinforme permite realizar esa consulta sql extra na que extraemos o nome, etc

## variables

accede con $V{nombre_variable}

ejecutan un cálculo sobre un field e devolven un valor

si se arrastra un field a unha banda pertencente a un grupo, tes a opción de crear variable; automáticamente modifica a variable para que se resetee según ese grupo (resetType), de esta forma a operación afecta a cada item do grupo

para operacións máis complejas, crea / modifica propiedades da variable manualmente

p.ej: tes variable precio_producto que calcula stock * precio, o informe mostra productos, esta variable mostra en cada producto o seu precio

por outro lado tes variable suma_total que calcula sum de $V{precio_producto}, esto pilla e suma todos os precio_producto que ocurriron (resetType = page, osea non se resetea)

# javabeans

crear widget customizado, extendendo da base, esta clase vai a ser un javabean

debe implementar Serializable, e ter constructor sin parámetros

para añadir propiedades, crear variable privada e xerar getter + setter

para añadir widget customizado á paleta: compilar, click dereito na clase -> tools -> add to palette, dentro de Beans

## complex properties

these properties are new classes that need to be Serializable too, and the variables inside need to have getters and setters too

you need to add no params constructor + a constructor with all info, this one will be used later in the customEditorSupport

to create a custom editor for these new properties, create a new JPanel, design the UI etc

add a public method that returns the property: `public CustomProperty getSelectedValue() {}`

finally you need to link the editor with the property:
    - create class that extends PropertyEditorSupport
    - contains a reference to the custom editor, let's call it customEditor
    - override some methods, read pdf
      - getJavaInitializationString(). This one seems redundant, you can already get the property with customEditor.getSelectedValue(). But you need to supply the info needed to copy paste a String with the java code that creates the property, when netbeans parses what you do in the GUI editor the custom java code it adds. Here you add java code that would create the Property with same info as the one you retrieved from the editor. That's why the property needs a constructor with all info.
    - right click the custom widget -> bean info editor
    - move to the designer tab, search for the custom property, add the editor support class

# JavaFX

- Stage = app window
  - Scene
    - node / graph element

loading images: create folder reosurces: `new Image("resources/speaker2.png");`

`choiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {}`

sometimes to add stuff you first get the arraylist then add to it:
`choiceBox.getItems().add()`

listener for text change in textField:
`textField.textProperty().addListener(new ChangeListener<String>`

## Tables

the object represented in the table must be created as a class, and every variable it has is a Property. It has a getter / setter for each property *value* + variableProperty() method that just returns the property itself

the list that holds the data of the table:
```
final ObservableList<Alumno> data = FXCollections.observableArrayList(
            new Alumno("Pepe", "Domínguez", "pepe@gmail.com")
);
```

add items to that list and stuff and table updates

columns can be created in SceneBuilder

then you do this on each column:
```
        column3.setCellValueFactory(new PropertyValueFactory<>("email"));
        column3.setCellFactory(TextFieldTableCell.<Alumno>forTableColumn());

        table.setItems(data);
        table.setEditable(true);
```

this step isn't in the pdf so memorize it:
`column.setCellFactory(TextFieldTableCell.<Alumno>forTableColumn());`

## popups

to get Stage from a widget:

`hbox.getScene().getWindow()`

Stage extends Window


creating a popup manually
```
Stage stage = new Stage();
stage.initOwner( hbox.getScene().getWindow());
stage.initModality(modality);
HBox hb = new HBox();
stage.setScene(new Scene(hb));
stage.showAndWait();
```

Alerts with custom ButtonTypes:
```
ButtonType bt2 = new ButtonType("2");
ButtonType bt3 = new ButtonType("3");

Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", bt1, bt2, bt3);
alert.setHeaderText("Press a button if you dare");
Optional<ButtonType> result = alert.showAndWait();
```

to return something custom from a Dialog:
```
dialog.setResultConverter((param) -> {
    if (param == ButtonType.CANCEL) return "x"
});

Optional<String> result = dialog.showAndWait();
```

keep in mind that there are premade ButtonTypes, like ButtonType.CANCEL

When creating a complex custom Dialog, keep in mind:
- contents of Dialog is a DialogPane, there you add stuff like:
  - actual content, can be another scene `dialogPane.setContent()`
  - buttons `dialogPane.getButtonTypes().add(buttonType)`
  - to get a button from buttonType: `dialogPane.lookupButton(buttonType)`

# Swing

window is: JFrame (main) or JDialog (secondary)

`JOptionPane.showConfirmDialog(null, text, "Error", JOptionPane.OK_CANCEL_OPTION);`

table exercise folder structure
- dto: the POJOs that hold the data
- gui: swing stuff
- loxica: holds the array with the POJOs used, or handles connection to the DB
- sql: sql files

```
    private void initTable() {
        jTableClientes.setModel(new StudentTableModelGeneric(this));

        jTableClientes.setRowHeight(22);

        jTableClientes.setAutoCreateRowSorter(true);
        jTableClientes.getColumnModel().getColumn(3).setCellEditor(new SpinnerEditor());
    }
```

custom SpinnerEditor:
```
public class SpinnerEditor extends DefaultCellEditor {
    private JSpinner spinner;

    public SpinnerEditor() {
        super(new JTextField());
        spinner = new JSpinner(new SpinnerNumberModel(0, 0, 150, 1));
        spinner.setBorder(null);
    }

    public Component getTableCellEditorComponent(
            JTable table, Object value, boolean isSelected, int row, int column) {
        spinner.setValue(value);
        return spinner;
    }

    public Object getCellEditorValue() {
        return spinner.getValue();
    }
}
```

hack to deal with tables, just reset everything on any changes:
```
StudentTableModelGeneric dtm = (StudentTableModelGeneric) jTableClientes.getModel();

dtm.clearTableModelData();

for (Student student : loxicaNegocio.getStudentList()) {
    dtm.addRow(student);
```

confirming when exiting app:
```
this.addWindowListener(new WindowAdapter() {
    public void windowClosing(WindowEvent e) {
        int i = JOptionPane.showConfirmDialog(null, "Seguro que quiere salir?");
        if (i == 0) {
            loxicaNegocio.closeConnection();
            System.exit(0);
        }
    }
});
```


sql stuff:
```
public void modifyStudent(Student student) throws SQLException {
    PreparedStatement statement = conn
            .prepareStatement("update student set dni = ?, name = ?, surname = ?, age = ? where dni = ?");

    statement.setString(1, student.getDni());
    statement.setString(2, student.getName());
    (...)

public ArrayList<Student> getStudentList() throws SQLException {
    ArrayList<Student> students = new ArrayList<>();
    PreparedStatement statement = conn.prepareStatement("select * from student");
    ResultSet data = statement.executeQuery();
    while (data.next()) {
        Student student = new Student(data.getString("dni"), data.getString("name"), data.getString("surname"),
                data.getInt("age"));
        students.add(student);
    }
```